
source ~/.fzf.zsh
#determines search program for fzf
if type ag &> /dev/null; then
    export FZF_DEFAULT_COMMAND='ag -p ~/.gitignore -g ""'
fi
#refer rg over ag
if type rg &> /dev/null; then
    export FZF_DEFAULT_COMMAND='rg --files --hidden'
fi

alias cdn="cd backup/d2/Claire_Desktop/notorious_aws/"
alias aws_repo="cd ~/repo/aws/"
alias gb="cd ~/repo/gitlab/"


alias az="cd ~/repo/gitlab/chatbox-client"


alias nc="cd ~/repo/aws/niimfer/niimfer-contracts"
alias note="cd ~/repo/gitlab/mac-m1-note"

alias ncgo="cd ~/repo/aws/niimfer/niimfer-contracts && ./scripts/start.sh"
alias nwgo="cd ~/repo/aws/niimfer/niimfer-web && yarn start"
alias nckill="cd ~/repo/aws/niimfer/niimfer-contracts && ./scripts/killprocess.sh"

alias ncvim="cd ~/repo/aws/niimfer/niimfer-contracts && nvim"
alias nwvim="cd ~/repo/aws/niimfer/niimfer-web && nvim"
alias nbvim="cd ~/repo/aws/niimfer/niimfer-backend && nvim"

alias tggo="cd ~/repo/aws/niimfer/niimfer-contracts && ./scripts/graph/start.sh"
alias tgp="cd ~/repo/aws/niimfer/niimfer-contracts && ./scripts/graph/start_prod.sh"
alias tgs="cd ~/repo/aws/niimfer/niimfer-contracts && ./scripts/graph/start_stag.sh"

alias initvim="nvim ~/.config/nvim/init.vim"
alias coc="nvim ~/.config/nvim/coc-settings.json"
alias sshvim="nvim ~/.ssh/config"
alias lg="lazygit"
alias nz="nvim ~/.zshrc"
alias nvinf="cd ~/repo/gitlab/aws-game-infra"

alias ym="code ~/repo/gitlab/young_man_app"
alias ymgo="code ~/repo/gitlab/young_man_go_backend"
alias cmd="cd ~/repo/gitlab/cmd"

# Amaze project
alias ob="code ~/repo/gitlab/openai_service_go_backend"
alias eb="code ~/repo/gitlab/backend-amaze-enterprise"
alias as="code ~/repo/gittea/amaze-account-service"
alias cs="code ~/repo/gittea/amaze-chatroom-service"
alias ssos="code ~/repo/gittea/self-service-ordering-system"
alias amaze="code ~/repo/gitlab/aiamze-app"
alias widget="code ~/repo/github/claire0418/openai-chat-widget"
alias amz="cd ~/repo/gittea/self-service-ordering-system && ./scripts/start.sh"
alias amzw="cd ~/repo/gittea/self-service-ordering-system && ./start_web.sh"
alias ap="code ~/repo/gitlab/aiamze-app"

alias color="code ~/repo/gitlab/color-detector"
alias cw="code ~/repo/github/claire0418/openai-chat-widget"

alias o2="code ~/repo/gitlab/o2-backend"
export PATH=$PATH:/usr/local/sbin


#    export LAZYGIT_NEW_DIR_FILE=~/.lazygit/newdir
    

#    lazygit "$@"

#    if [ -f $LAZYGIT_NEW_DIR_FILE ]; then
#            cd "$(cat $LAZYGIT_NEW_DIR_FILE)"
#            rm -f $LAZYGIT_NEW_DIR_FILE >07/ /dev/null
#    fi

#if [ "$TMUX" = "" ]; then tmux; fi


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#HOMEBREW RABBITMQ
export HOMEBREW_RABBITMQ=/usr/local/Cellar/rabbitmq/3.7.11/sbin/
export PATH=$PATH:$HOMEBREW_RABBITMQ


export PATH=$PATH:/usr/local/sbin

# GO
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:/usr/local/go/bin:$PATH

export PATH=/opt/homebrew/bin:$PATH
export PATH=$PATH:/usr/local/sbin




autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /opt/homebrew/bin/terraform terraform
export PATH=${PATH}:/usr/local/mysql/bin/
alias start_mysql="sudo /usr/local/mysql/support-files/mysql.server start --skip-grant-tables"
alias stop_mysql="sudo /usr/local/mysql/support-files/mysql.server stop"

# MySQL
export PATH="/usr/local/mysql/bin:$PATH"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/claire/opt/anaconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/claire/opt/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/claire/opt/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/claire/opt/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<



PATH=/bin:/usr/bin:/usr/local/bin:${PATH}
export PATH
eval "$(~/bin/rtx activate zsh)"

. /opt/homebrew/opt/asdf/libexec/asdf.sh
PATH=$(go env GOPATH)/bin:$PATH 